﻿using System.Web.Mvc;
using System.Web.Routing;

namespace LittleURL
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute(
				name: "Redirect",
				url: "{shortUrl}",
				defaults: new { controller = "Home", action = "RedirectPage" });

			routes.MapRoute(
				name: "Default",
				url: "{controller}/{action}/{id}",
				defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
			);
		}
	}
}
