﻿using System;
using System.Web.Mvc;
using LittleURL.Util;
using LittleURL.Models;

namespace LittleURL.Controllers
{
	public class HomeController : Controller
	{
		// GET: Home
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult GetLittleURL(string url)
		{
			if (string.IsNullOrWhiteSpace(url))
			{
				return Json(new { Success = false, Url = "", Message = "" });
			}

			URLDataProvider objUrlDataProvider = new URLDataProvider();
			long lId = objUrlDataProvider.FindOrInsertURL(url);
			Base36Converter objBase36Converter = new Base36Converter();
			string strUrlCode = objBase36Converter.Encode(lId);

			string strFinalUrl = (Request.Url.Scheme + Uri.SchemeDelimiter + Request.Url.Host + (Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port)) + "/" + strUrlCode;

			return Json(new { Success = true, Url = strFinalUrl, Message = "" }, JsonRequestBehavior.AllowGet);
		}

		public ActionResult RedirectPage(string shortUrl)
		{
			Base36Converter objBase36Converter = new Base36Converter();
			long lUrlID = objBase36Converter.Decode(shortUrl);
			return Redirect(new URLDataProvider().GetOriginalUrl(lUrlID));
		}
	}
}