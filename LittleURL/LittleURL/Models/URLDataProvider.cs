﻿using System;
using System.Linq;

namespace LittleURL.Models
{
	public class URLDataProvider
	{
		static DB_A0B32F_UrlEntities objContext = new DB_A0B32F_UrlEntities();

		public long FindOrInsertURL(string strUrl)
		{
			long lId = 0L;

			var objUrl = objContext.LittleURLs.Where(item => item.URL.Equals(strUrl, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

			if (objUrl != null)
			{
				lId = objUrl.Id;
			}
			else
			{
				objContext.LittleURLs.Add(new LittleURL() { URL = strUrl });
				objContext.SaveChanges();
				lId = objContext.LittleURLs.Where(item => item.URL.Equals(strUrl, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().Id;
			}

			return lId;
		}


		public string GetOriginalUrl(long lId)
		{
			var objUrl = objContext.LittleURLs.Where(item => item.Id == lId).FirstOrDefault();
			return objUrl.URL;
		}
	}
}