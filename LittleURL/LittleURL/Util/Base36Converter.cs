﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LittleURL.Util
{
	public class Base36Converter
	{
		public static readonly string Alphabet = "abcdefghijklmnopqrstuvwxyz0123456789";
		public static readonly int Base = Alphabet.Length;

		public string Encode(long lUrlID)
		{
			if (lUrlID == 0) return Alphabet[0].ToString();

			string s = string.Empty;

			while (lUrlID > 0)
			{
				s += Alphabet[(int)(lUrlID % Base)];
				lUrlID = lUrlID / Base;
			}

			return string.Join(string.Empty, s.Reverse());
		}

		public long Decode(string sUrlCode)
		{
			var lUrlId = 0;

			foreach (var c in sUrlCode)
			{
				lUrlId = (lUrlId * Base) + Alphabet.IndexOf(c);
			}

			return lUrlId;
		}
	}
}